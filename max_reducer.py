#!/usr/bin/env python3
import sys

max_length = 10
max_pairs = []

for line in sys.stdin:
    line = line.strip()
    word, count = line.split('\t', 1)
    count = int(count)
    pair = (count, word)

    if len(max_pairs) < max_length:
        max_pairs.append(pair)
        max_pairs = sorted(max_pairs)
        continue

    smallest_pair = max_pairs[0]
    if count > smallest_pair[0]:
        max_pairs[0] = pair
        max_pairs = sorted(max_pairs)

for pair in sorted(max_pairs, reverse=True):
    print(f'{pair[1]}\t{pair[0]}')
