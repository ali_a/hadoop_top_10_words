#!/urs/bin/env python3
from PyPDF2 import PdfFileReader

pdf_file = open('./book.pdf', 'rb')
book = PdfFileReader(pdf_file)

text = ''
for page in range(31, book.numPages):
    text += book.getPage(page).extractText()

pdf_file.close()

with open('./book.txt', 'w') as f:
    f.write(text)
